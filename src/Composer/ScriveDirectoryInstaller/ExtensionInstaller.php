<?php

namespace Composer\ScriveDirectoryInstaller;

use Composer\Package\PackageInterface;
use Composer\Installer\LibraryInstaller;

class ExtensionInstaller extends LibraryInstaller
{
    /**
     * {@inheritDoc}
     */
    public function getPackageBasePath( PackageInterface $package )
    {
        $prefix = substr( $package->getPrettyName(), 0, 26 );
        if ( 'codepile/scrive-extension-' !== $prefix )
        {
            throw new \InvalidArgumentException( 'Unable to install Extension, codepile extensions ' . 'should always start their package name with ' . '"codepile/scrive-extension"' );
        }
        
        return '../.scrive/ext/' . substr( $package->getPrettyName(), 26 );
    }
    
    /**
     * {@inheritDoc}
     */
    public function supports( $packageType )
    {
        return 'scrive-extension' === $packageType;
    }
}