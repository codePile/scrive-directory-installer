<?php

namespace Composer\ScriveDirectoryInstaller;

use Composer\Composer;
use Composer\IO\IOInterface;
use Composer\Plugin\PluginInterface;
use Composer\ScriveDirectoryInstaller\ExtensionInstaller;

class ExtensionInstallerPlugin implements PluginInterface
{
    public function activate( Composer $composer, IOInterface $io )
    {
        $installer = new ExtensionInstaller( $io, $composer );
        $composer->getInstallationManager()->addInstaller( $installer );
    }
}