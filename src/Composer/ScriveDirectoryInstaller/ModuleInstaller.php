<?php

namespace Composer\ScriveDirectoryInstaller;

use Composer\Package\PackageInterface;
use Composer\Installer\LibraryInstaller;

class ModuleInstaller extends LibraryInstaller
{
    /**
     * {@inheritDoc}
     */
    public function getPackageBasePath( PackageInterface $package )
    {
        $prefix = substr( $package->getPrettyName(), 0, 23 );
        if ( 'codepile/scrive-module-' !== $prefix )
        {
            throw new \InvalidArgumentException( 'Unable to install Module, codepile modules ' . 'should always start their package name with ' . '"codepile/scrive-module-"' );
        }
        
        return '../.scrive/modules/' . substr( $package->getPrettyName(), 23 );
    }
    
    /**
     * {@inheritDoc}
     */
    public function supports( $packageType )
    {
        return 'scrive-module' === $packageType;
    }
}