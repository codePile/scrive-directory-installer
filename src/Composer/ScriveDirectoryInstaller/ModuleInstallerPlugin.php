<?php

namespace Composer\ScriveDirectoryInstaller;

use Composer\Composer;
use Composer\IO\IOInterface;
use Composer\Plugin\PluginInterface;
use Composer\ScriveDirectoryInstaller\ModuleInstaller;

class ModuleInstallerPlugin implements PluginInterface
{
    public function activate( Composer $composer, IOInterface $io )
    {
        $installer = new ModuleInstaller( $io, $composer );
        $composer->getInstallationManager()->addInstaller( $installer );
    }
}