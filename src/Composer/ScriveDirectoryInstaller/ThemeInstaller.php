<?php

namespace Composer\ScriveDirectoryInstaller;

use Composer\Package\PackageInterface;
use Composer\Installer\LibraryInstaller;

class ThemeInstaller extends LibraryInstaller
{
    /**
     * {@inheritDoc}
     */
    public function getPackageBasePath( PackageInterface $package )
    {
        $prefix = substr( $package->getPrettyName(), 0, 22 );
        if ( 'codepile/scrive-theme-' !== $prefix )
        {
            throw new \InvalidArgumentException( 'Unable to install Theme, codepile themes ' . 'should always start their package name with ' . '"codepile/scrive-theme-"' );
        }
        
        return '../themes/' . substr( $package->getPrettyName(), 22 );
    }
    
    /**
     * {@inheritDoc}
     */
    public function supports( $packageType )
    {
        return 'scrive-theme' === $packageType;
    }
}