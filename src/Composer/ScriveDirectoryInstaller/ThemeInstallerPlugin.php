<?php

namespace Composer\ScriveDirectoryInstaller;

use Composer\Composer;
use Composer\IO\IOInterface;
use Composer\Plugin\PluginInterface;
use Composer\ScriveDirectoryInstaller\ThemeInstaller;

class ThemeInstallerPlugin implements PluginInterface
{
    public function activate( Composer $composer, IOInterface $io )
    {
        $installer = new ThemeInstaller( $io, $composer );
        $composer->getInstallationManager()->addInstaller( $installer );
    }
}